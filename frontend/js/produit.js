const url_page = new URL(window.location.href);
const search = new URLSearchParams(url_page.search);
const id_produit = search.get("id");

const url = `http://localhost:3000/api/furniture/${id_produit}`;

const monMeuble = async () => {
    const response = await fetch(url);
    const meuble = await response.json();
    return meuble;
    
}

monMeuble().then((meuble) => {
    
    remplissageCardDetaillée(meuble);

    const bouton = document.querySelector(".btn");
    bouton.addEventListener('click',() => {
        ajoutProduitsPanier(meuble);
    });
});

const remplissageCardDetaillée = (product) => {
    const template = document.querySelector("#produit_detaille");
    const clone = document.importNode(template.content,true);
    const produit = document.querySelector(".fiche_produit");

    const imageCard = clone.querySelector(".card-img-top");
    imageCard.src = product.imageUrl;

    const nameCard = clone.querySelector(".card-name");
    nameCard.innerHTML = product.name;

    const priceCard = clone.querySelector(".card-price");
    priceCard.innerHTML = product.price/100 + " €";

    const decriptionCard = clone.querySelector(".description");
    decriptionCard.innerHTML = product.description;

    const listeVernis = clone.querySelector(".form-select");
    (product.varnish).forEach(vernis => {
        const element_liste = document.createElement("option");
        element_liste.innerHTML = vernis;
        listeVernis.appendChild(element_liste);
    });



    produit.appendChild(clone);


;}
const ajoutProduitsPanier = (produit) => {
    const quantity = Math.abs(parseInt(document.querySelector(".form-control").value));
    if(!localStorage.getItem("panier")){ // création du panier dans le localstorage qui contient un tableu de meubles
        listeProduit =[];
        produit.quantite = quantity;
        listeProduit.push(produit)
        localStorage.setItem("panier", JSON.stringify(listeProduit));
        alert(`Panier créé et ${quantity} produit(s) ajouté(s).`);
    }
    else{
        listeProduct = JSON.parse(localStorage["panier"]);
        if(produitPresent(produit,listeProduct)){
            listeProduct.forEach(product =>{ 
                if(produit._id == product._id){ //trouve le produit correspondant dans le panier
                    product.quantite += quantity;
                    localStorage.setItem("panier", JSON.stringify(listeProduct));
                    alert(`${quantity} produit(s) en plus dans le panier`)
                }
            })
        }
        else {
            produit.quantite = quantity;
            listeProduct.push(produit);
            localStorage.setItem("panier", JSON.stringify(listeProduct));
            alert(`${quantity} produit(s) ajouté(s) au panier.`);
        }
    }
};
const produitPresent = (produit, listeProduit) => {
    estPresent = false;
        listeProduit.forEach(product =>{
            if(produit._id == product._id){
                estPresent = true;
            }
        });
    return estPresent;
};
