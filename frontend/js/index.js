
let url = "http://localhost:3000/api/furniture";

const dataMeubles = async () => {
    const response = await fetch(url);
    const meubles = await response.json();
    return meubles;
}
dataMeubles().then((meubles) => {
    const template = document.querySelector("#produit");
   
    meubles.forEach(meuble => {
        const produit = document.querySelector(".fiche_produits");
        const clone = document.importNode(template.content,true);

        const card = clone.querySelector(".card");
        card.href = `html/produit.html?id=${meuble._id}`;

        const imageCard = clone.querySelector(".card-img-top");
        imageCard.src = meuble.imageUrl;

        const nameCard = clone.querySelector(".card-name");
        nameCard.innerHTML = meuble.name;

        const priceCard = clone.querySelector(".card-price");
        priceCard.innerHTML = meuble.price/100 + " €";

        produit.appendChild(clone);
    }); 
    
    console.log(meubles); // test unitaire
});
