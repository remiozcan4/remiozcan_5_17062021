let url = "http://localhost:3000/api/furniture/order";

// retourne la liste des produit présent dans le localStorage.
const listePanier = ()=>{ 
    let liste = [];
    for(let key in localStorage)
    {
        if(key =="panier"){
            liste = JSON.parse(localStorage[key]);
        }
    }
    return liste;
}
const listeStringID = () =>{// retourne une liste d'ID des produit du panier. 
    listeProductID = [];
    listePanier().forEach(produit => {
        listeProductID.push(produit._id);
    });
    return listeProductID;
}
const remplissageInfoPanier = (meubles,listeProduit) => {// rempli la listeProduit avec les informations de chaque produit dans le template Card.
    const template = document.querySelector("#produit_panier");
    (meubles).forEach(meuble => {
        const clone = document.importNode(template.content,true);
    
        const element_liste = document.createElement("li");
        element_liste.className = "list-group-item";
    
        const nameCard = clone.querySelector(".card-name");
        nameCard.innerHTML = meuble.name + ` (${meuble.price/100}€)`;

        const quantity = clone.querySelector(".quantiteInput");
        quantity.value = meuble.quantite;
        
        const priceCard = clone.querySelector(".card-price");
        priceCard.innerHTML = (meuble.price/100)*meuble.quantite + " €";
        
        element_liste.appendChild(clone);
        listeProduit.appendChild(element_liste);
    });
    const prixTotal = document.querySelector(".prixTotal");
    prixTotal.innerHTML = `Prix total : ${calculPrixTotal(meubles)} €`
};
const calculPrixTotal = (meubles) =>{
    let prixTotal = 0;
    meubles.forEach(meuble => {
        prixTotal += (meuble.price*meuble.quantite);
    })
    return prixTotal/100;
};
const supprimeMeuble = (meubleASupprimer,listeMeuble) =>{
    listeMeuble.forEach((meuble,index) =>{
        if(meubleASupprimer._id == meuble._id){
            listeMeuble.splice(index,1);
        }
    });
    return listeMeuble;
}
const supprimerProduitPanier = (meubleASupprimer) => { // s'il y a 1 seul produit dans le panier, on supprime le panier sinon on supprime le meuble du panier
    panier = listePanier();
    if(panier.length === 1){
        localStorage.removeItem("panier");
    }
    else{
        nouveauPanier = supprimeMeuble(meubleASupprimer,panier);
        localStorage.setItem("panier",JSON.stringify(nouveauPanier));   
    } 
};

const infoForm = () => { 
   const champsFormulaire = document.querySelectorAll(".form-control")
   const contact = {
       firstName : champsFormulaire[0].value,
       lastName : champsFormulaire[1].value,
       email : champsFormulaire[2].value,
       address : champsFormulaire[3].value,
       city : champsFormulaire[4].value
 };
   const products = listeStringID();
    return {
        contact,products
    };
};
const envoiForm = async() => {
   const reponse = await fetch(url,{
        method: "POST",
        headers: { 
            'Accept': 'application/json', 
            'Content-Type': 'application/json' 
        },
        body: JSON.stringify(infoForm())
    })
    return reponse.json();
};
const contientChiffre = (chaine) => {
    let hasNumber = false;
        for(i=0; i <= 9; i++){
            if(chaine.includes(i)){
                hasNumber = true;
            }
        }   
    return hasNumber
}
const controlForm = () => {
    let valide = "ok";
    const inputs = document.querySelectorAll(".form-control");
    inputs.forEach((entree,index) => {
        if(entree.value == ""){
             valide = "vide";
        }
        if( index < 2 || index === 4){// Si l'entree = Prénom , Nom ou Ville
             if(contientChiffre(entree.value)){
                valide = "chiffre";  
            }
        }
        if(index ===2 ){
            if(!entree.value.includes("@")){
                valide = "email";
            }
        }        
    })
    return valide;
}


//--------MAIN------------
meubles = listePanier();
const listeProduit = document.querySelector(".list-group");
const form = document.querySelector(".formulaire");
    if(meubles.length > 0){
        remplissageInfoPanier(meubles,listeProduit);

        const quantiteInputs = document.querySelectorAll(".quantiteInput");
        quantiteInputs.forEach((input,index) => {
            input.addEventListener('change', chagementQuantite);
            function chagementQuantite(e){
                if(e.target.value > 0){
                    meubles[index].quantite = e.target.value;
                }
                else{ // prends la valeur absolue si l'utilisateur rentre nombre négatif
                   meubles[index].quantite = Math.abs(e.target.value);
                }               
                localStorage.setItem("panier",JSON.stringify(meubles));
                window.location.reload();           
            }
        })

        const boutonSupprimes = document.querySelectorAll(".btn-secondary");
        boutonSupprimes.forEach((bouton,index)=> {            
            bouton.addEventListener('click',() => {
                supprimerProduitPanier(meubles[index]);
            })
        })

        const boutonEnvoi = document.querySelector(".btn-primary");
        boutonEnvoi.addEventListener('click',function(event) {
            event.preventDefault();
            if(controlForm()== "ok"){
                envoiForm().then((reponse) => { // reponse = objet contact, le tableau produits et orderId (string)
                    if(reponse.orderId == undefined){
                        console.log("pas d'orderId");
                    }
                    else{
                        window.location = `confirmation.html?orderId=${reponse.orderId}&nom=${reponse.contact.firstName}`;
                    }
                    
                });
            }
            else{
                event.preventDefault();
                switch(controlForm()){
                    case "vide" :
                        alert("Champs requis nécessaire");
                        break;

                    case "chiffre" : 
                        alert("Pas de chiffre dans le champs Nom, Prénom ou Ville.");
                        break;   

                    case "email" :
                        alert("Veuillez rentrer un e-mail valide.")
                        break;    
                }
                
            }
           
        })
    }
    else{
        form.style.display ="none";
        listeProduit.innerHTML = "Pas de produit dans le panier, cliquez sur Articles disponibles pour commencer vos achats."
    }

