const url_page = new URL(window.location.href);
const search = new URLSearchParams(url_page.search);
const id_order = search.get("orderId");
const nom = search.get("nom");

const listePanier = () => {
    let liste = [];
    for(let key in localStorage)
    {
        if(key =="panier"){
            liste = JSON.parse(localStorage[key]);
        }
    }
    return liste;
}
const calculPrixTotal = (meubles) => {
    let prixTotal = 0;
    meubles.forEach(meuble => {
        prixTotal += (meuble.price*meuble.quantite);
    })
    return prixTotal/100;
};
const nombreArticles = (meubles) => {
    let nombreArticles = 0;
    meubles.forEach(meuble => {
        nombreArticles += parseInt(meuble.quantite);
    })
    return nombreArticles;
};
meublesAchetés = listePanier();
const prixTotal = calculPrixTotal(meublesAchetés);
const message = document.querySelector("p");
const merci = document.querySelector("h1");
const nombreArcticles = nombreArticles(meublesAchetés);

merci.innerHTML = `<br>Merci ${nom}, votre commande a bien été enregistrée.`
message.innerHTML = `<br><strong>Récapitulatif de la commande:</strong><br>
<br>Nombre d'article(s) : ${nombreArcticles}<br>
Prix : <strong>${prixTotal} €</strong><br>
Id : ${id_order}`;

const boutonNew = document.querySelector("#nouvelleCommande");
boutonNew.addEventListener('click',()=>{
    localStorage.clear();
})

